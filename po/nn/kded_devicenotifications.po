# Translation of kded_devicenotifications to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-05 00:39+0000\n"
"PO-Revision-Date: 2023-07-29 12:08+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: devicenotifications.cpp:278
#, kde-format
msgid "%1 has been plugged in."
msgstr "%1 vart kopla til."

#: devicenotifications.cpp:278
#, kde-format
msgid "A USB device has been plugged in."
msgstr "Ei USB-eining vart kopla til."

#: devicenotifications.cpp:281
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Detected"
msgstr "USB-eining oppdaga"

#: devicenotifications.cpp:299
#, kde-format
msgid "%1 has been unplugged."
msgstr "%1 vart kopla frå."

#: devicenotifications.cpp:299
#, kde-format
msgid "A USB device has been unplugged."
msgstr "Ei USB-eining vart kopla frå."

#: devicenotifications.cpp:302
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Removed"
msgstr "USB-eining fjerna"
