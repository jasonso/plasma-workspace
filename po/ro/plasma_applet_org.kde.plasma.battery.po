# Traducerea plasma_applet_battery.po în Română
# translation of plasma_applet_battery to Romanian
# Copyright (C) 2008 This_file_is_part_of_KDE
# This file is distributed under the same license as the plasma_applet_battery package.
# Laurenţiu Buzdugan <lbuz@rolix.org>, 2008".
# Sergiu Bivol <sergiu@cip.md>, 2008, 2009, 2010, 2011, 2012, 2013, 2020, 2021, 2022, 2023, 2024.
# Cristian Oneț <onet.cristian@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_battery\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-04 00:40+0000\n"
"PO-Revision-Date: 2024-02-24 18:30+0000\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/ui/BatteryItem.qml:107
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:171
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""
"Sănătatea acestui acumulator este la numai %1% și trebuie înlocuit. "
"Contactați producătorul."

#: package/contents/ui/BatteryItem.qml:191
#, kde-format
msgid "Time To Full:"
msgstr "Timp până la umplere:"

#: package/contents/ui/BatteryItem.qml:192
#, kde-format
msgid "Remaining Time:"
msgstr "Timp rămas:"

#: package/contents/ui/BatteryItem.qml:197
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr "Se estimează…"

#: package/contents/ui/BatteryItem.qml:209
#, kde-format
msgid "Battery Health:"
msgstr "Sănătate acumulator:"

#: package/contents/ui/BatteryItem.qml:215
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:229
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr "Acumulatorul e configurat să se încarce până la aproximativ %1%."

#: package/contents/ui/CompactRepresentation.qml:111
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/logic.js:23 package/contents/ui/logic.js:29
#: package/contents/ui/main.qml:228
#, kde-format
msgid "Fully Charged"
msgstr "Încărcat deplin"

#: package/contents/ui/logic.js:28
#, kde-format
msgid "Discharging"
msgstr "Se descarcă"

#: package/contents/ui/logic.js:30
#, kde-format
msgid "Charging"
msgstr "Se încarcă"

#: package/contents/ui/logic.js:32
#, kde-format
msgid "Not Charging"
msgstr "Nu se încarcă"

#: package/contents/ui/logic.js:35
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "Absent"

#: package/contents/ui/main.qml:121
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr "Miniaplicația de acumulator a activat inhibiția întregului sistem"

#: package/contents/ui/main.qml:133
#, fuzzy, kde-format
#| msgctxt "Minimize the length of this string as much as possible"
#| msgid "Manually block sleep and screen locking"
msgid "Failed to block automatic sleep and screen locking"
msgstr "Blochează manual adormirea și blocarea ecranului"

#: package/contents/ui/main.qml:147
#, fuzzy, kde-format
#| msgctxt "Minimize the length of this string as much as possible"
#| msgid "Manually block sleep and screen locking"
msgid "Failed to unblock automatic sleep and screen locking"
msgstr "Blochează manual adormirea și blocarea ecranului"

#: package/contents/ui/main.qml:159 package/contents/ui/main.qml:197
#: package/contents/ui/main.qml:203
#, kde-format
msgid "Power Management"
msgstr "Gestiunea alimentării"

#: package/contents/ui/main.qml:176
#, kde-format
msgid "Failed to activate %1 mode"
msgstr "Activarea regimului %1 a eșuat"

#: package/contents/ui/main.qml:203
#, kde-format
msgid "Power and Battery"
msgstr "Alimentare și acumulator"

#: package/contents/ui/main.qml:235
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr "Acumulator la %1%, nu se încarcă"

#: package/contents/ui/main.qml:237
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr "Acumulator la %1%, conectat dar oricum se descarcă"

#: package/contents/ui/main.qml:239
#, kde-format
msgid "Battery at %1%, Charging"
msgstr "Acumulator la %1%, se încarcă"

#: package/contents/ui/main.qml:242
#, kde-format
msgid "Battery at %1%"
msgstr "Acumulator la %1%"

#: package/contents/ui/main.qml:250
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr ""
"Sursa de alimentare nu e suficient de puternică ca să încarce acumulatorul"

#: package/contents/ui/main.qml:254
#, kde-format
msgid "No Batteries Available"
msgstr "Niciun acumulator disponibil"

#: package/contents/ui/main.qml:260
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr "%1 până la încărcare deplină"

#: package/contents/ui/main.qml:262
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr "%1 rămase"

#: package/contents/ui/main.qml:265
#, kde-format
msgid "Not charging"
msgstr "Nu se încarcă"

#: package/contents/ui/main.qml:269
#, fuzzy, kde-format
#| msgid "Automatic sleep and screen locking are disabled"
msgid ""
"Automatic sleep and screen locking are disabled; middle-click to re-enable"
msgstr "Adormirea și blocarea ecranului automate sunt dezactivate"

#: package/contents/ui/main.qml:271
#, fuzzy, kde-format
#| msgctxt "Minimize the length of this string as much as possible"
#| msgid "Manually block sleep and screen locking"
msgid "Middle-click to disable automatic sleep and screen locking"
msgstr "Blochează manual adormirea și blocarea ecranului"

#: package/contents/ui/main.qml:276
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] "O aplicație a cerut activarea regimului de performanță"
msgstr[1] "%1 aplicații au cerut activarea regimului de performanță"
msgstr[2] "%1 de aplicații au cerut activarea regimului de performanță"

#: package/contents/ui/main.qml:280
#, fuzzy, kde-format
#| msgid "System is in Performance mode"
msgid "System is in Performance mode; scroll to change"
msgstr "Sistemul e în regim de performanță"

#: package/contents/ui/main.qml:284
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] "O aplicație a cerut activarea regimului de economisire"
msgstr[1] "%1 aplicații au cerut activarea regimului de economisire"
msgstr[2] "%1 de aplicații au cerut activarea regimului de economisire"

#: package/contents/ui/main.qml:288
#, fuzzy, kde-format
#| msgid "System is in Power Save mode"
msgid "System is in Power Save mode; scroll to change"
msgstr "Sistemul e în regim de economisire"

#: package/contents/ui/main.qml:291
#, kde-format
msgid "System is in Balanced Power mode; scroll to change"
msgstr ""

#: package/contents/ui/main.qml:383
#, kde-format
msgid "&Show Energy Information…"
msgstr "Arată informații de&spre energie…"

#: package/contents/ui/main.qml:389
#, kde-format
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr ""
"Arată procentajul acumulatorului pe pictogramă când nu e încărcat complet"

#: package/contents/ui/main.qml:402
#, kde-format
msgid "&Configure Power Management…"
msgstr "&Configurează gestiunea alimentării…"

#: package/contents/ui/PowerManagementItem.qml:41
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr "Blochează manual adormirea și blocarea ecranului"

#: package/contents/ui/PowerManagementItem.qml:76
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"Laptopul e configurat să nu doarmă la închiderea capacului în timp ce este "
"conectat un monitor extern."

#: package/contents/ui/PowerManagementItem.qml:87
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] "%1 aplicație blochează acum adormirea și blocarea ecranului:"
msgstr[1] "%1 aplicații blochează acum adormirea și blocarea ecranului:"
msgstr[2] "%1 de aplicații blochează acum adormirea și blocarea ecranului:"

#: package/contents/ui/PowerManagementItem.qml:107
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr "%1 blochează acum adormirea și blocarea ecranului (%2)"

#: package/contents/ui/PowerManagementItem.qml:109
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr "%1 blochează acum adormirea și blocarea ecranului (motiv necunoscut)"

#: package/contents/ui/PowerManagementItem.qml:111
#, kde-format
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr "O aplicație blochează acum adormirea și blocarea ecranului (%1)"

#: package/contents/ui/PowerManagementItem.qml:113
#, kde-format
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr ""
"O aplicație blochează acum adormirea și blocarea ecranului (motiv necunoscut)"

#: package/contents/ui/PowerManagementItem.qml:117
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:119
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr "%1: motiv necunoscut"

#: package/contents/ui/PowerManagementItem.qml:121
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr "Aplicație necunoscută: %1"

#: package/contents/ui/PowerManagementItem.qml:123
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr "Aplicație necunoscută: motiv necunoscut"

#: package/contents/ui/PowerProfileItem.qml:37
#, kde-format
msgid "Power Save"
msgstr "Economisirea energiei"

#: package/contents/ui/PowerProfileItem.qml:41
#, kde-format
msgid "Balanced"
msgstr "Echilibrat"

#: package/contents/ui/PowerProfileItem.qml:45
#, kde-format
msgid "Performance"
msgstr "Performanță"

#: package/contents/ui/PowerProfileItem.qml:62
#, kde-format
msgid "Power Profile"
msgstr "Profil de alimentare"

#: package/contents/ui/PowerProfileItem.qml:97
#, kde-format
msgctxt "Power profile"
msgid "Not available"
msgstr "Indisponibil"

#: package/contents/ui/PowerProfileItem.qml:192
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""
"Regimul de performanță a fost dezactivat pentru a reduce generarea de "
"căldură deoarece calculatorul a depistat că s-ar putea afla în poala "
"dumneavoastră."

#: package/contents/ui/PowerProfileItem.qml:194
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr ""
"Regimul de performanță nu e disponibil deoarece calculatorul e prea "
"fierbinte."

#: package/contents/ui/PowerProfileItem.qml:196
#, kde-format
msgid "Performance mode is unavailable."
msgstr "Regimul de performanță nu e disponibil."

#: package/contents/ui/PowerProfileItem.qml:209
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""
"Performanța poate fi scăzută pentru a reduce generarea de căldură deoarece "
"calculatorul a depistat că s-ar putea afla în poala dumneavoastră."

#: package/contents/ui/PowerProfileItem.qml:211
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr "Performanța poate fi redusă deoarece calculatorul e prea fierbinte."

#: package/contents/ui/PowerProfileItem.qml:213
#, kde-format
msgid "Performance may be reduced."
msgstr "Performanța poate fi redusă."

#: package/contents/ui/PowerProfileItem.qml:224
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] "O aplicație a cerut activarea %2:"
msgstr[1] "%1 aplicații au cerut activarea %2:"
msgstr[2] "%1 de aplicații au cerut activarea %2:"

#: package/contents/ui/PowerProfileItem.qml:242
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerProfileItem.qml:261
#, kde-kuit-format
msgid ""
"Power profiles may be supported on your device.<nl/>Try installing the "
"<command>power-profiles-daemon</command> package using your distribution's "
"package manager and restarting the system."
msgstr ""
"Profilurile de alimentare pot fi disponibile pentru acest dispozitiv.<nl/"
">Încercați să instalați pachetul <command>power-profiles-daemon</command> "
"folosind gestionarul de pachete al distribuției și să reporniți sistemul."

#~ msgctxt "Placeholder is brightness percentage"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery and Brightness"
#~ msgstr "Acumulator și luminozitate"

#~ msgid "Brightness"
#~ msgstr "Luminozitate"

#~ msgid "Battery"
#~ msgstr "Acumulator"

#~ msgid "Scroll to adjust screen brightness"
#~ msgstr "Derulați pentru a ajusta luminozitatea ecranului"

#~ msgid "Display Brightness"
#~ msgstr "Luminozitate ecran"

#~ msgid "Keyboard Brightness"
#~ msgstr "Luminozitate tastatură"

#, fuzzy
#~| msgid "Performance mode is unavailable."
#~ msgid "Performance mode has been manually enabled"
#~ msgstr "Regimul de performanță nu e disponibil."

#~ msgid "General"
#~ msgstr "Generale"

#~ msgid "An application is preventing sleep and screen locking:"
#~ msgstr "O aplicație previne adormirea și blocarea ecranului:"

#~ msgctxt "short symbol to signal there is no battery currently available"
#~ msgid "-"
#~ msgstr "-"

#, fuzzy
#~| msgid "More..."
#~ msgid "Configure Power Saving..."
#~ msgstr "Mai multe..."

#~ msgid "Time To Empty:"
#~ msgstr "Timp până la epuizare:"

#, fuzzy
#~| msgid "Charging"
#~ msgid "%1% Charging"
#~ msgstr "Se încarcă"

#, fuzzy
#~| msgctxt "tooltip"
#~| msgid "Plugged in"
#~ msgid "%1% Plugged in"
#~ msgstr "Atașat"

#, fuzzy
#~| msgid "Capacity:"
#~ msgctxt "The degradation in the battery's energy capacity"
#~ msgid "Capacity degradation:"
#~ msgstr "Capacitate:"

#, fuzzy
#~| msgctxt "Placeholder is battery percentage"
#~| msgid "%1%"
#~ msgctxt "Placeholder is battery's capacity degradation"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgctxt "Placeholder is battery capacity"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Vendor:"
#~ msgstr "Vânzător:"

#~ msgid "Model:"
#~ msgstr "Model:"

#~ msgctxt "Placeholder is battery name"
#~ msgid "%1:"
#~ msgstr "%1:"

#~ msgid "N/A"
#~ msgstr "Indisp."

#~ msgid "1 hour "
#~ msgid_plural "%1 hours "
#~ msgstr[0] "1 oră "
#~ msgstr[1] "%1 ore "
#~ msgstr[2] "%1 de ore "

#~ msgid "1 minute"
#~ msgid_plural "%1 minutes"
#~ msgstr[0] "1 minut"
#~ msgstr[1] "%1 minute"
#~ msgstr[2] "%1 de minute"

#, fuzzy
#~| msgid "AC Adapter:"
#~ msgid "AC Adapter"
#~ msgstr "Adaptor AC:"

#, fuzzy
#~| msgid "Plugged in"
#~ msgid "Plugged In"
#~ msgstr "Conectat"

#, fuzzy
#~| msgid "Not plugged in"
#~ msgid "Not Plugged In"
#~ msgstr "Neconectat"

#, fuzzy
#~| msgctxt "Label for remaining time"
#~| msgid "Time Remaining:"
#~ msgid "Time remaining until full: %1"
#~ msgstr "Timp rămas:"

#, fuzzy
#~| msgctxt "Label for remaining time"
#~| msgid "Time Remaining:"
#~ msgid "Time remaining until empty: %1"
#~ msgstr "Timp rămas:"

#~ msgid "%1% (charged)"
#~ msgstr "%1% (încărcat)"

#~ msgctxt "tooltip"
#~ msgid "AC Adapter:"
#~ msgstr "Adaptor AC:"

#~ msgctxt "tooltip"
#~ msgid "<b>Plugged in</b>"
#~ msgstr "<b>Atașat</b>"

#~ msgctxt "tooltip"
#~ msgid "<b>Not plugged in</b>"
#~ msgstr "<b>Nu este atașat</b>"

#~ msgctxt "overlay on the battery, needs to be really tiny"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Configure Battery Monitor"
#~ msgstr "Configurare Monitor acumulator"

#~ msgctxt "Suspend the computer to RAM; translation should be short"
#~ msgid "Sleep"
#~ msgstr "Adormire"

#~ msgctxt "Suspend the computer to disk; translation should be short"
#~ msgid "Hibernate"
#~ msgstr "Hibernare"

#~ msgid "<b>%1% (charged)</b>"
#~ msgstr "<b>%1% (încărcat)</b>"

#~ msgid "<b>%1% (discharging)</b>"
#~ msgstr "<b>%1% (descărcare)</b>"

#~ msgid "<b>%1% (charging)</b>"
#~ msgstr "<b>%1% (încărcare)</b>"

#~ msgctxt "Battery is not plugged in"
#~ msgid "<b>Not present</b>"
#~ msgstr "<b>Absent</b>"

#~ msgid "Show the state for &each battery present"
#~ msgstr "Afiș&ează starea fiecărui acumulator prezent"

#~ msgid "<b>Battery:</b>"
#~ msgstr "<b>Acumulator:</b>"

#~ msgctxt "tooltip"
#~ msgid "<b>AC Adapter:</b>"
#~ msgstr "<b>Adaptor AC:</b>"

#~ msgctxt "tooltip"
#~ msgid "Not plugged in"
#~ msgstr "Nu este atașat"

#~ msgid "Show remaining time for the battery"
#~ msgstr "Arată timpul rămas pentru acumulator"

#~ msgctxt "tooltip: placeholder is the battery ID"
#~ msgid "<b>Battery %1:</b>"
#~ msgstr "<b>Acumulator %1:</b> "

#~ msgctxt "Suspend the computer to disk; translation should be short"
#~ msgid "Inhibit"
#~ msgstr "Inhibă"

#~ msgid "Inhibit"
#~ msgstr "Inhibă"

#, fuzzy
#~| msgid "AC Adapter:"
#~ msgid "AC Adapter: "
#~ msgstr "Adaptor AC:"

#~ msgid "<b>Battery:</b> "
#~ msgstr "<b>Acumulator:</b> "

#~ msgctxt "Shown when a time estimate is not available"
#~ msgid "%1% (discharging)"
#~ msgstr "%1% (descărcare)"

#, fuzzy
#~| msgctxt "Shown when a time estimate is not available"
#~| msgid "%1% (discharging)"
#~ msgctxt "state of battery"
#~ msgid "%1% (discharging)"
#~ msgstr "%1% (descărcare)"

#~ msgid "%2% (discharging)"
#~ msgstr "%2% (descărcare)"

#~ msgid "%2% (charging)"
#~ msgstr "%2% (încărcare)"

#~ msgid "Actions"
#~ msgstr "Acțiuni"

#~ msgid "<b>Battery:</b> %1% (charging)<br />"
#~ msgstr "<b>Acumulator:</b> %1% (încărcare)<br />"

#~ msgid "<b>Battery %1:</b> %2% (fully charged)<br />"
#~ msgstr "<b>Acumulator %1:</b> %2% (încărcat deplin)<br />"

#~ msgid "<b>Battery %1:</b> %2% (discharging)<br />"
#~ msgstr "<b>Acumulator %1:</b> %2% (descărcare)<br />"

#~ msgid "<b>AC Adapter:</b> Not plugged in"
#~ msgstr "<b>Adaptor AC:</b> detașat"

#~ msgid "<b>1 hour 1 minute</b> remaining<br />"
#~ msgid_plural "<b>1 hour %1 minutes</b> remaining<br />"
#~ msgstr[0] "<b>1 oră 1 minut</b> rămase<br />"
#~ msgstr[1] "<b>1 oră %1 minute</b> rămase<br />"
#~ msgstr[2] "<b>1 oră %1 de minute</b> rămase<br />"

#~ msgid "<b>%1 hour %1 minute</b> remaining<br />"
#~ msgid_plural "<b>%1 hours %2 minute</b> remaining<br />"
#~ msgstr[0] "<b>%1 oră %1 minute</b> rămase<br />"
#~ msgstr[1] "<b>%1 ore %2 minute</b> rămase<br />"
#~ msgstr[2] "<b>%1 ore %2 de minute</b> rămase<br />"

#~ msgid "<b>%1 hour %2 minutes</b> remaining<br />"
#~ msgid_plural "<b>%1 hours %2 minutes</b> remaining<br />"
#~ msgstr[0] "<b>%1 oră %2 minute</b> rămase<br />"
#~ msgstr[1] "<b>%1 ore %2 minute</b> rămase<br />"
#~ msgstr[2] "<b>%1 ore %2 de minute</b> rămase<br />"

#~ msgid "<b>%1 hours</b> remaining<br />"
#~ msgstr "<b>%1 ore</b> rămase<br />"

#~ msgid "<b>%1 minute</b> remaining<br />"
#~ msgid_plural "<b>%1 minutes</b> remaining<br />"
#~ msgstr[0] "<b>%1 minut</b> rămas<br />"
#~ msgstr[1] "<b>%1 minute</b> rămase<br />"
#~ msgstr[2] "<b>%1 de minute</b> rămase<br />"

#~ msgid "Oxygen theme"
#~ msgstr "Tema Oxygen"

#~ msgid "Classic theme"
#~ msgstr "Tema clasică"

#~ msgid "sources"
#~ msgstr "surse"

#~ msgid "Percent"
#~ msgstr "Procent"

#~ msgid "n/a"
#~ msgstr "indisponibil"

#, fuzzy
#~| msgid "Battery"
#~ msgid ""
#~ "Battery:\n"
#~ "CPU:"
#~ msgstr "Acumulator"

#~ msgid "Always show &background"
#~ msgstr "Arată fun&dalul întotdeauna"
