# Uyghur translation for plasma_applet_lockout.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sahran <sahran.ug@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_lockout\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-19 01:53+0000\n"
"PO-Revision-Date: 2013-09-08 07:05+0900\n"
"Last-Translator: Gheyret Kenji <gheyret@gmail.com>\n"
"Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr ""

#: contents/ui/ConfigGeneral.qml:33
#, kde-format
msgctxt ""
"Heading for a list of actions (leave, lock, switch user, hibernate, suspend)"
msgid "Show actions:"
msgstr ""

#: contents/ui/ConfigGeneral.qml:34
#, kde-format
msgid "Log Out"
msgstr ""

#: contents/ui/ConfigGeneral.qml:41
#, kde-format
msgid "Shut Down"
msgstr ""

#: contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Restart"
msgstr ""

#: contents/ui/ConfigGeneral.qml:53 contents/ui/data.js:5
#, kde-format
msgid "Lock"
msgstr "قۇلۇپلا"

#: contents/ui/ConfigGeneral.qml:59
#, fuzzy, kde-format
#| msgid "Switch user"
msgid "Switch User"
msgstr "ئىشلەتكۈچى ئالماشتۇر"

#: contents/ui/ConfigGeneral.qml:65 contents/ui/data.js:46
#, kde-format
msgid "Hibernate"
msgstr "ئۈچەك"

#: contents/ui/ConfigGeneral.qml:71 contents/ui/data.js:39
#, fuzzy, kde-format
#| msgid "Sleep"
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "ئۇخلات"

#: contents/ui/data.js:6
#, kde-format
msgid "Lock the screen"
msgstr "ئېكراننى قۇلۇپلايدۇ"

#: contents/ui/data.js:12
#, kde-format
msgid "Switch user"
msgstr "ئىشلەتكۈچى ئالماشتۇر"

#: contents/ui/data.js:13
#, kde-format
msgid "Start a parallel session as a different user"
msgstr "باشقا ئىشلەتكۈچى سۈپىتىدە پاراللېل ئەڭگىمە باشلايدۇ"

#: contents/ui/data.js:18
#, kde-format
msgid "Shutdown…"
msgstr ""

#: contents/ui/data.js:19
#, fuzzy, kde-format
#| msgid "Logout, turn off or restart the computer"
msgid "Turn off the computer"
msgstr "كومپيۇتېرنى تىزىمدىن چىقار، تاقا ياكى قايتا قوزغات"

#: contents/ui/data.js:25
#, kde-format
msgid "Restart…"
msgstr ""

#: contents/ui/data.js:26
#, kde-format
msgid "Reboot the computer"
msgstr ""

#: contents/ui/data.js:32
#, kde-format
msgid "Logout…"
msgstr ""

#: contents/ui/data.js:33
#, kde-format
msgid "End the session"
msgstr ""

#: contents/ui/data.js:40
#, kde-format
msgid "Sleep (suspend to RAM)"
msgstr "ئۇخلا(RAM غا توڭلىتىدۇ)"

#: contents/ui/data.js:47
#, kde-format
msgid "Hibernate (suspend to disk)"
msgstr "ئۈچەك (دىسكىغا توڭلىتىدۇ)"

#~ msgid "Do you want to suspend to disk (hibernate)?"
#~ msgstr "سىز سىستېمىنى دىسكىغا توڭلىتامسىز(ئۈچەك)؟"

#~ msgid "Yes"
#~ msgstr "ھەئە"

#~ msgid "No"
#~ msgstr "ياق"

#~ msgid "Do you want to suspend to RAM (sleep)?"
#~ msgstr "سىز سىستېمىنى ئەسلەككە توڭلىتامسىز(ئۇخلات)؟"

#~ msgid "Leave"
#~ msgstr "ئايرىل"

#~ msgid "Leave..."
#~ msgstr "ئايرىل…"

#, fuzzy
#~| msgid "Actions"
#~ msgctxt "Heading for list of actions (leave, lock, shutdown, ...)"
#~ msgid "Actions"
#~ msgstr "مەشغۇلاتلار"

#~ msgid "Suspend"
#~ msgstr "توڭلات"

#~ msgid "Configure Lock/Logout"
#~ msgstr "قۇلۇپلاش/چىقىشنى سەپلە"
